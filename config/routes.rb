Rails.application.routes.draw do
  get 'calorie_calculator/index'

  resources :sessions, only: [:new, :create, :destroy]
  
  get 'signup', to: 'users#new', as: 'signup'
  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'Calorie Calculator', to: 'calorie_calculator#index', as: 'calorie_calculator'

  resources :users
  resources :calorie_calculator
  get 'page/index'


  root 'page#index'
end
